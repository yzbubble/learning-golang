// 位操作符示例
// 启发：位操作心智模型可以将每一个二进制操作数的每一位用从0开始的序号表示，顺序从右到左，这样就可以把一整个二进制操作数简单映射为一个序号集合。
// 在做这种简单映射后，重新理解位操作符，解释如下：
// A & B  -> 将两个集合共有的元素提取出来
// A | B  -> 将两个集合合并
// A ^ B  -> 排除两个集合共有元素，合并两个集合剩下的元素
// A &^ B -> A集合排除B集合元素
package main

import "fmt"

func main() {
	var x uint8 = 1<<1 | 1<<5
	var y uint8 = 1<<1 | 1<<2

	fmt.Printf("%08b\n", x) // "00100010", the set {1, 5}
	fmt.Printf("%08b\n", y) // "00000110", the set {1, 2}

	fmt.Printf("%08b\n", x&y)  // "00000010", the intersection {1}
	fmt.Printf("%08b\n", x|y)  // "00100011", the union {1, 2, 5}
	fmt.Printf("%08b\n", x^y)  // "00100010", the symmetric difference {2, 5}
	fmt.Printf("%08b\n", x&^y) // "00100000", the difference {5}

	for i := uint(0); i < 8; i++ {
		if x&(1<<i) != 0 { // membership test
			fmt.Println(i) // "1", "5"
		}
	}

	fmt.Printf("%08b\n", x<<1) // "01000100", the set {2, 6}
	fmt.Printf("%08b\n", x>>1) // "00010001", the set {0, 4}
}
