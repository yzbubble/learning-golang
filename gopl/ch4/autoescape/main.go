// 演示html/template如何转义html特殊字符
package main

import (
	"fmt"
	"log"
	"html/template"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "text/html;charset=utf-8")
		const templ = `<p>A: {{.A}}</p><p>B: {{.B}}</p>`
		t := template.Must(template.New("escape").Parse(templ))
		var data struct {
			A string // unstructed plain text
			B template.HTML // trusted HTML
		}
		data.A = "<b>Hello!</b>"
		data.B = "<b>Hello!</b>"
		if err := t.Execute(w, data); err != nil {
			log.Fatal(err)
		}
	})
	fmt.Println("localhost:8000 starting ...")
	http.ListenAndServe("localhost:8000", nil)
}