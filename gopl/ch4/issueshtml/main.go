package main

import (
	"fmt"
	"os"
	"log"
	"html/template"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "text/html;charset=utf-8")
		result, err := SearchIssues(os.Args[1:])
		if err != nil {
			log.Fatal(err)
		}
		if err := issueList.Execute(w, result); err != nil {
			log.Fatal(err)
		}
	})
	fmt.Println("localhost:8000 starting ...")
	http.ListenAndServe("localhost:8000", nil)
}

var issueList = template.Must(template.New("issuelist").Parse(`
<h1>{{.TotalCount}} issues</h1>
<table>
<tr style='text-align: left'>
  <th>#</th>
  <th>State</th>
  <th>User</th>
  <th>Title</th>
</tr>
{{range .Items}}
<tr>
  <td><a href='{{.HTMLURL}}'>{{.Number}}</a></td>
  <td>{{.State}}</td>
  <td><a href='{{.User.HTMLURL}}'>{{.User.Login}}</a></td>
  <td><a href='{{.HTMLURL}}'>{{.Title}}</a></td>
</tr>
{{end}}
</table>
`))