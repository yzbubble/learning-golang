// 示例说明：Map的value类型也可以是一个聚合类型，比如是一个map或slice。

package main

import (
	"fmt"
)

var graph = make(map[string]map[string]bool)

func main() {
	addEage("zhangsan", "dog1")

	fmt.Printf("graph[\"zhangsan\"][\"dog1\"] = %v\n", graph["zhangsan"]["dog1"])
	fmt.Printf("graph[\"zhangsan\"][\"cat1\"] = %v\n", graph["zhangsan"]["cat1"])
	fmt.Printf("graph[\"lisi\"][\"dog1\"] = %v\n", graph["lisi"]["dog1"])

	// 即使map为nil，通过键值引用值依然是个有意义的值
	var foo map[string]bool = nil
	fmt.Printf("foo[\"hello\"] = %v", foo["hello"])
}

func addEage(from, to string) {
	edges := graph[from]
	if edges == nil {
		edges = make(map[string]bool)
		graph[from] = edges
	}
	edges[to] = true
}

func hasEdge(from, to string) bool {
	return graph[from][to]
}