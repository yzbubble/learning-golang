// 演示defer的常见使用场景
package main

import (
	"net/http"
	"path"
	"os"
	"io"
	"fmt"
)

func main() {
	filename, _, err := fetch(os.Args[1]);
	if err != nil {
		fmt.Errorf("%s", err)
	}
	fmt.Printf("output file name: %s\n", filename)

}

func fetch(url string) (filename string, n int64, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", 0, err
	}
	defer resp.Body.Close()
	local := path.Base(resp.Request.URL.Path)
	fmt.Printf("local: %s\n", local)
	if local == "/"  || local == "." {
		local = "index.html"
	}
	f, err := os.Create(local)
	if err != nil {
		return "", 0, err
	}
	n, err = io.Copy(f, resp.Body)
	if closeErr := f.Close(); err == nil {
		err = closeErr
	}
	return local, n, err
}
