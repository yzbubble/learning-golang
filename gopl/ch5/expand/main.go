package main

import (
	"strings"
	"fmt"
)

func main() {
	fmt.Println(expand("hello foo!foo! hello foo!", "foo", func(s string) string{
		return "FOOD"
	}))
	fmt.Println(expand("你好啊 foo!foo! 你好啊 foo!", "foo", func(s string) string{
		return "FOOD"
	}))
	fmt.Println(expand("你好啊 foo!foo! 你好啊 foo!", "foo", nil))
}

func expand(s string, substr string, f func(string) string) string {
	if f == nil {
		return s
	}
	var result string
	for index := strings.Index(s, substr); index != -1; index = strings.Index(s, substr) {
		result += s[:index]
		result += f(substr)
		s = s[index + len(substr):];
	}
	result += s
	return result
}
